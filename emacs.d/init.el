;;; init.el --- user init file      -*- no-byte-compile: t -*-

;; http://worace.works/2016/06/07/getting-started-with-emacs-for-ruby/

;;; Packages setup

;;(setq package-enable-at-startup nil)


(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

; fetch the list of packages available
(unless package-archive-contents
  (package-refresh-contents))

; list the packages you want
(setq package-list '(projectile-rails
		     pastelmac-theme
		     nord-theme
		     magit
		     leuven-theme
		     challenger-deep-theme
		     base16-theme
		     better-defaults
                     helm
                     helm-projectile
                     helm-ag
		     ruby-electric
;;                     seeing-is-believing
		     rbenv
                     inf-ruby
                     markdown-mode
                     markdown-preview-mode
                     smartparens))

; install the missing packages
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; appearance

(setq custom-safe-themes t)
(load-theme 'pastelmac t)

(global-linum-mode 1)
(global-hl-line-mode 1)
(blink-cursor-mode 0)
(tool-bar-mode -1)
(show-paren-mode 1)


(set-face-attribute 'default nil
		    :family "PragmataPro Mono"
		    :foundry "fsdf"
		    :slant 'normal
		    :weight 'normal
		    :height 90
		    :width 'normal)


(global-visual-line-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)
(setq inhibit-startup-message t)


;; no tabs etc
(setq tab-width 2
      indent-tabs-mode nil)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq require-final-newline t)

;;; behaviour
(setq ring-bell-function 'ignore)
(setq load-prefer-newer t)

(setq vc-follow-symlinks t)

(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.saves"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups

(savehist-mode 1)

;;;fuzzy completion
(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "s-f") #'helm-projectile-ag)
(global-set-key (kbd "s-t") #'helm-projectile-find-file-dwim)

;; i wonder if this will be helpful, or annoying
(require 'smartparens-config)

;;; ruby stuff;;;
(global-rbenv-mode)
(rbenv-use-global)
;; Autoclose paired syntax elements like parens, quotes, etc
(add-hook 'ruby-mode-hook 'ruby-electric-mode)

(add-to-list 'auto-mode-alist
             '("\\.\\(?:cap\\|gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" . ruby-mode))
(add-to-list 'auto-mode-alist
             '("\\(?:Brewfile\\|Capfile\\|Gemfile\\(?:\\.[a-zA-Z0-9._-]+\\)?\\|[rR]akefile\\)\\'" . ruby-mode))

;; gem install seeing_is_believing
;; (setq seeing-is-believing-prefix "C-.")
;; (add-hook 'ruby-mode-hook 'seeing-is-believing)
;; (require 'seeing-is-believing)

(autoload 'inf-ruby-minor-mode "inf-ruby" "Run an inferior Ruby process" t)
(add-hook 'ruby-mode-hook 'inf-ruby-minor-mode)

;; magit
(setq magit-last-seen-setup-instructions "1.4.0")
(global-set-key (kbd "C-x g") 'magit-status)



;; general


;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(blink-cursor-mode nil)
;;  '(custom-safe-themes
;;    (quote
;;     ("1012cf33e0152751078e9529a915da52ec742dabf22143530e86451ae8378c1a" default)))
;;  '(package-selected-packages
;;    (quote
;;     (use-package projectile-rails pastelmac-theme nord-theme magit leuven-theme challenger-deep-theme base16-theme)))
;;  '(tool-bar-mode nil))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:family "PragmataPro Mono" :foundry "fsdf" :slant normal :weight normal :height 90 :width normal)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#231f20" "#ee2e24" "#00853e" "#ffd204" "#009ddc" "#98005d" "#009ddc" "#d9d8d8"])
 '(ansi-term-color-vector
   [unspecified "#231f20" "#ee2e24" "#00853e" "#ffd204" "#009ddc" "#98005d" "#009ddc" "#d9d8d8"] t)
 '(blink-cursor-mode nil)
 '(custom-enabled-themes (quote (parchment)))
 '(custom-safe-themes
   (quote
    ("dbd3619fc88d2703da45c4e25329324d04f526b7cf63eaf4aea038fe77a81292" "9f08dacc5b23d5eaec9cccb6b3d342bd4fdb05faf144bdcd9c4b5859ac173538" "51043b04c31d7a62ae10466da95a37725638310a38c471cc2e9772891146ee52" "030346c2470ddfdaca479610c56a9c2aa3e93d5de3a9696f335fd46417d8d3e4" "886fe9a7e4f5194f1c9b1438955a9776ff849f9e2f2bbb4fa7ed8879cdca0631" default)))
 '(hl-todo-keyword-faces
   (quote
    (("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#3a81c3")
     ("OKAY" . "#3a81c3")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#42ae2c")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX" . "#dc752f")
     ("XXXX" . "#dc752f")
     ("???" . "#dc752f"))))
 '(package-selected-packages
   (quote
    (parchment-theme kaolin-themes spacemacs-theme gitconfig-mode smartparens markdown-preview-mode markdown-mode ruby-electric helm-ag helm-projectile helm better-defaults use-package rbenv projectile-rails pastelmac-theme nord-theme magit leuven-theme challenger-deep-theme base16-theme)))
 '(pdf-view-midnight-colors (quote ("#655370" . "#fbf8ef")))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
